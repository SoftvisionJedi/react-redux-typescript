import React from 'react';
import './App.css';
import Posts from './components/posts';
import PostForm from './components/post.form';
import { Provider } from 'react-redux';
import RootStore from './redux/store';

const store = new RootStore().create();

export default class App extends React.Component<any, any> {
	constructor(props: any){
		super(props);
		this.state = {}
	}

	render() {
		return (
			<Provider store={store}>
				<div className="App">
					<PostForm></PostForm>
					<hr/>
					<hr/>
					<Posts></Posts>
				</div>
			</Provider>
		)
	}
}
