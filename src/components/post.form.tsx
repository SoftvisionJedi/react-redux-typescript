
import * as React from 'react';
import {IPost} from '../interfaces/post';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'
import { connect } from 'react-redux';
import PostActions from '../redux/actions/post.actions';

class PostForm extends React.Component<any, any> {
	constructor(props: any) {
		super(props);
		this.state = {
			item: {
				id: 0,
				userId: 12,
				title: '',
				body: ''
			} as IPost,
			show: false
		};

		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);
	}

  handleShow() {
    this.setState({ show: true });
	}

	handleClose() {
    this.setState({ show: false });
	}
	
	onChange = (evt: React.ChangeEvent<any>): any => {
		const state = {...this.state};
		state.item[evt.target.name] = evt.target.value;
		this.setState({state});
	}

	onSubmit = (evt: React.FormEvent): any => {
		evt.preventDefault();

		const post = {
			title: this.state.item.title,
			body: this.state.item.body,
			userId: 12
		};

		this.props.createPost(post);

		this.setState({
			item: {
				id: 0,
				userId: 12,
				title: '',
				body: ''
			},
			show: false
		});
	}
	
	render() {
		return (
			<>
			<Button variant="primary" onClick={this.handleShow}>
				Add A New Post
			</Button>

			<Modal show={this.state.show} onHide={this.handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Add A New Post</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						<Form.Group controlId="formTitle">
							<Form.Label>Post Title</Form.Label>
							<Form.Control required name="title" onChange={this.onChange} type="text" placeholder="Enter Your Post Title" />
						</Form.Group>
						<Form.Group controlId="formBody">
							<Form.Label>Post Content</Form.Label>
							<Form.Control required name="body" onChange={this.onChange} as="textarea" rows="3" type="text" placeholder="What do you want to say..." />
						</Form.Group>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={this.handleClose}>
						Close
					</Button>
					<Button variant="primary" type="submit" onClick={this.onSubmit}>
						Save Post
					</Button>
				</Modal.Footer>
			</Modal>
		</>


		);
	}
}

export default connect(null, { createPost: PostActions.Inst.createPost })(PostForm);