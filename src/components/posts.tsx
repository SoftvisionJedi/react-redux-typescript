import * as React from 'react';
import { connect } from 'react-redux';
import PostActions from '../redux/actions/post.actions';

import {IPost} from '../interfaces/post';
import {IReducerState} from '../interfaces/state';
import {IPostProps} from '../interfaces/props';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export class Posts extends React.Component<any, any> {

	componentWillMount() {
		this.props.fetchPosts();
	}

	componentWillReceiveProps(nextProps: IPostProps) {
		if(nextProps.newPost) {
			this.props.posts.unshift(nextProps.newPost);
		}
	}

	render() {
		const postItems = this.props.posts.map((post: IPost) => {
			return (
				<div key={post.id}>
					<Card border="primary" text="dark">
						<Card.Header as="h4">{post.title}</Card.Header>
						<Card.Body>
							<Card.Title>By User ID: {post.userId}</Card.Title>
							<Card.Text>{post.body}</Card.Text>
						</Card.Body>
					</Card>
					<br/>
				</div>
			)
		});
		return (
			<div>
				<h1> Posts </h1>
				{postItems}
			</div>
			
		);
	}
}

const  mapStateToProps = (state: IReducerState): IPostProps => ({
	posts: state.posts.items,
	newPost: state.posts.item
});

export default connect(mapStateToProps, { fetchPosts: PostActions.Inst.fetchPosts })(Posts);