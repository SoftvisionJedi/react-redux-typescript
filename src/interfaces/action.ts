import {Action} from 'redux';

import {IPost} from './post';
export interface IPostAction extends Action {
	payload: IPost | Array<IPost>;
}
