
export interface IPostRes {
	id: number;
	title: string;
	body: string;
}

export interface IPost extends IPostRes {
	userId: number;
}