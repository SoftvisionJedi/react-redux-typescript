import {IPost} from './post';

export interface IProps {
	name: string;
	value: any;
}

export interface IPostProps {
	posts: Array<IPost>;
	newPost: IPost;
}