import {IPost} from './post';

export interface IReducerState {
	posts: IPostState;
	fetchPosts: Function;
	createPost: Function;
}
export interface IPostState {
	items: Array<IPost>;
	item: IPost;
}