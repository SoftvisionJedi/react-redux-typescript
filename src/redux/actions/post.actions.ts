import {Dispatch} from 'react';

import {AnyAction} from 'redux';

import {IPost, IPostRes} from '../../interfaces/post';

import {FETCH_POSTS, NEW_POST, EDIT_POST} from './types';

export default class PostActions {
	private static _instance: PostActions;

	public static get Inst() {
		if (!this._instance) {
			this._instance = new this();
		}
		return this._instance;
	}

	public fetchPosts = () => (dispatch: Dispatch<AnyAction>): void => {
		fetch('http://jsonplaceholder.typicode.com/posts')
			.then((res: any) => res.json())
			.then((posts: Array<IPost>) => {
				dispatch({
					type: FETCH_POSTS,
					payload: posts
				})
			})
			.catch((ex: Error) => {
				dispatch({
					type: FETCH_POSTS,
					error: ex
				})
			})
	}

	public createPost = (post: IPost) => (dispatch: Dispatch<AnyAction>): void => {
		fetch('http://jsonplaceholder.typicode.com/posts', {
			method: 'POST', 
			headers: {
				'content-type': 'application/json'
			},
			body: JSON.stringify(post)
		})
			.then((res: any) => res.json())
			.then((post: IPostRes) => {
				dispatch({
					type: NEW_POST,
					payload: post
				})
			})
			.catch((ex: Error) => {
				dispatch({
					type: FETCH_POSTS,
					error: ex
				})
			})
	}

	public editPost = (post: IPost) => (dispatch: Dispatch<AnyAction>): void => {
		dispatch({
			type: EDIT_POST,
			payload: post
		})
	}

	private constructor() {
		
	}
}