import { FETCH_POSTS, NEW_POST, EDIT_POST } from '../actions/types';
import { IPostAction } from "../../interfaces/action";
import { IPost } from '../../interfaces/post';
import { IPostState } from '../../interfaces/state';

const initialState: IPostState = {
	items: [],
	item: {} as IPost
}

export default (state: IPostState = initialState, action: IPostAction) => {
	switch(action.type) {
		case FETCH_POSTS: { 
			return {
				...state,
				items: action.payload
			}
		 }
		case NEW_POST:
		case EDIT_POST: { 
			return {
				...state,
				item: action.payload
			}
		}
		default: { return state; }
	}
}