import { createStore, applyMiddleware, Store, Middleware } from 'redux';

import thunk from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from '../reducers';

export default class RootStore {
	
	public store: any;
	public initialState: any;
	public middleWare: Array<Middleware>;

	constructor() {
		this.middleWare = new Array<Middleware>();
		this.middleWare.push(thunk);
	}

	public create(): Store {
		return createStore(rootReducer, this.initialState, composeWithDevTools(applyMiddleware(...this.middleWare)));
	}
}